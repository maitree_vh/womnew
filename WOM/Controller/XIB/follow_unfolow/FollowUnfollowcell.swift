//
//  FollowUnfollowcell.swift
//  WOM
//
//  Created by DECODER on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class FollowUnfollowcell: UITableViewCell {

    @IBOutlet weak var btnfollow: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnfollow.layer.cornerRadius = btnfollow.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
