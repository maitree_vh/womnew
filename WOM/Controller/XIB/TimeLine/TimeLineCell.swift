//
//  TimeLineCell.swift
//  WOM
//
//  Created by Maitree on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class TimeLineCell: UITableViewCell {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var womView: UIView!
    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var shadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
