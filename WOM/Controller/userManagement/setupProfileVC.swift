//
//  setupProfileVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit
import IQKeyboardManager

class setupProfileVC: UIViewController,UITextFieldDelegate,CustomeDatePickerDelegate,CustomePickerDelegate {
   

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnOpenImage: UIButton!
    @IBOutlet weak var txtName: RdtextField!
    @IBOutlet weak var txtDate: RdtextField!
    @IBOutlet weak var txtGender: RdtextField!
    @IBOutlet weak var btnCreate: UIButton!
    
    var datePicker : DatePicker?
    var genderPicker : CustomePicker?
    
    var genderArray = ["Male","Female","Prefer Not To Say"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtDate.layer.cornerRadius = txtDate.frame.height / 2
        txtDate.clipsToBounds = true
        
        txtName.layer.cornerRadius = txtName.frame.height / 2
        txtName.clipsToBounds = true
        
        txtGender.layer.cornerRadius = txtGender.frame.height / 2
        txtGender.clipsToBounds = true
        
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        
        txtGender.delegate = self
        txtDate.delegate = self
        
        datePicker?.Delegate = self
        genderPicker?.Delegate = self
        
        datePicker = Bundle.main.loadNibNamed("DatePicker", owner: self, options: nil)?.first as? DatePicker
        genderPicker = Bundle.main.loadNibNamed("CustomePicker", owner: self, options: nil)?.first as? CustomePicker
        
        txtDate.inputView = datePicker
        txtGender.inputView = genderPicker
        
        btnCreate.layer.cornerRadius = btnCreate.frame.height / 2
        btnCreate.clipsToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnCreate.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    //MARK:- textField Delegate metods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtDate {
            
            IQKeyboardManager.shared().isEnableAutoToolbar = false
            datePicker?.pickerView.datePickerMode = .date
            
            
        }else if textField == txtGender {
            
            IQKeyboardManager.shared().isEnableAutoToolbar = false
            genderPicker?.DataSource = genderArray
            
        }else{
            
            IQKeyboardManager.shared().isEnableAutoToolbar = true
        }
       
    }
    
    
    //MARK:- custom date picker & custom picker view
    func DidSelect(pickerView: UIPickerView, dataAt Index: Int) {
        
        txtGender.text = genderArray[Index]
        self.view.endEditing(true)
    }
    
    func DidCancel(PickerView: UIPickerView) {
        self.view.endEditing(true)
    }
    
    func DidSelect(pickerView: UIDatePicker, date: Date) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        txtDate.text = dateFormatter.string(from: date)
        self.view.endEditing(true)
        
    }
    
    func DidCancel(PickerView: UIDatePicker) {
        self.view.endEditing(true)
    }
    
    
    
    //MARK:- UIbutton Action
    @IBAction func btnActionCreate(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "friendsOnWomVC") as? friendsOnWomVC else {return}
        self.navigationController?.show(vc, sender: nil)
    }
    
    @IBAction func btnActionOpenImage(_ sender: Any) {
        
        
        
    }
    

}
