//
//  changeMobileNumberVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class changeMobileNumberVC: UIViewController {

    @IBOutlet weak var txtView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtView.layer.cornerRadius = txtView.frame.height / 2
        txtView.clipsToBounds = true
        
        btnContinue.layer.cornerRadius = btnContinue.frame.height / 2
        btnContinue.clipsToBounds = true
        
        txtView.giveBottomShadow(color: UIColor.darkGray)
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnContinue.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    //MARK:- UIbutton Action
    @IBAction func btnActionContinue(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}
