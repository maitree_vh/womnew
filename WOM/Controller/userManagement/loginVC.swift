//
//  loginVC.swift
//  WOM
//
//  Created by Decoder on 14/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class loginVC: UIViewController {

    
    @IBOutlet weak var faceBookView: UIView!
    @IBOutlet weak var gmailView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        faceBookView.layer.cornerRadius = faceBookView.frame.height / 2
        faceBookView.clipsToBounds = true
        
        gmailView.layer.cornerRadius = gmailView.frame.height / 2
        gmailView.clipsToBounds = true
        
        faceBookView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(btnFaceBook(_:))))
        gmailView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(btnGmail(_:))))
    }
    
    //MARK:- UIbutton Action
    @objc func btnFaceBook(_ tap : UITapGestureRecognizer){
        
        
    }
    
    @objc func btnGmail(_ tap : UITapGestureRecognizer){
        
        
    }
    
    @IBAction func btnActionCreate(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "setupProfileVC") as? setupProfileVC else {return}
        self.navigationController?.show(vc, sender: nil)
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        
        
    }
    
    
}
