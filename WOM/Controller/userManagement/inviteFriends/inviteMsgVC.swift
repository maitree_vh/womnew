//
//  inviteMsgVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class inviteMsgVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnGotIt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.darkGray.cgColor
        mainView.layer.borderWidth = 1
        mainView.clipsToBounds = true
        
        btnGotIt.layer.cornerRadius = btnGotIt.frame.height / 2
        btnGotIt.clipsToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnGotIt.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    //MARK:- UIbutton Actions
    @IBAction func btnActionGotIt(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "inviteMsgSecondVC") as? inviteMsgSecondVC else {return}
        self.navigationController?.show(vc, sender: nil)
        
    }
    
}
