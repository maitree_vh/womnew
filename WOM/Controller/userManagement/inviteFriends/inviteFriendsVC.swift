//
//  inviteFriendsVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class inviteFriendsVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btninviteFriends: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.giveBottomShadow(color: UIColor.darkGray)
        mainView.clipsToBounds = true
        
        btnSkip.layer.cornerRadius = btnSkip.frame.height / 2
        btnSkip.clipsToBounds = true
        
        btninviteFriends.layer.cornerRadius = btninviteFriends.frame.height / 2
        btninviteFriends.clipsToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btninviteFriends.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    //MARK:- UIbutton Actions
    @IBAction func btnActionFriends(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "secondInviteFriendsVC") as? secondInviteFriendsVC else {return}
        self.navigationController?.show(vc, sender: nil)
        

    }
    
    @IBAction func btnActionSkip(_ sender: Any) {
        
    }
    
    
   

}
