//
//  inviteMsgSecondVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class inviteMsgSecondVC: UIViewController {

    
    @IBOutlet weak var btnInviteFriends: UIButton!
    @IBOutlet weak var btnskip: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnInviteFriends.layer.cornerRadius = btnInviteFriends.frame.height / 2
        btnInviteFriends.clipsToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnInviteFriends.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    
    @IBAction func btnActionInviteFriends(_ sender: Any) {
    }
    
    @IBAction func btnActionSkip(_ sender: Any) {
    }
    


}
