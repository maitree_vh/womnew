//
//  secondInviteFriendsVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class secondInviteFriendsVC: UIViewController {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnGooglePlay: UIButton!
    @IBOutlet weak var btnAppleStore: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.giveBottomShadow(color: UIColor.darkGray)
        mainView.clipsToBounds = true
        
        imgLogo.isUserInteractionEnabled = true
        imgLogo.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(openVC(_:))))
        
    }
    
    //MARK:- UIbutton Action
    
    @objc func openVC(_ tap : UITapGestureRecognizer){
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "inviteMsgVC") as? inviteMsgVC else {return}
        
        self.navigationController?.show(vc, sender: nil)
        
    }
    
    
    @IBAction func btnActionGoogle(_ sender: Any) {
        
                if let name = URL(string: "https://www.youtube.com/watch?v=hMy5za-m5Ew"), !name.absoluteString.isEmpty {
                    let objectsToShare = [name]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    self.present(activityVC, animated: true, completion: nil)
                }
        
    }
    
    @IBAction func btnActionApple(_ sender: Any) {
    
                if let name = URL(string: "https://www.youtube.com/watch?v=hMy5za-m5Ew"), !name.absoluteString.isEmpty {
                    let objectsToShare = [name]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    self.present(activityVC, animated: true, completion: nil)
                }
        
    }
    
    
}
