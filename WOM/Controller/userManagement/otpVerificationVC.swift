//
//  otpVerificationVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit
import SVPinView

class otpVerificationVC: UIViewController {

    
    @IBOutlet weak var lblOTP: UILabel!
    @IBOutlet weak var otpView: SVPinView!
    @IBOutlet weak var btnChangeNumber: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    
    var otp = ""
    var timer : Timer?
    var time = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnResend.isHidden = true
        
        btnVerify.layer.cornerRadius = btnVerify.frame.height / 2
        btnVerify.clipsToBounds = true
        
        otpView.style = .underline
        otpView.keyboardType = .numberPad
        
        otpView.didChangeCallback = { pin in
            self.otp = pin
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(dropTime), userInfo: nil, repeats: true)
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnVerify.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    //MARK:- UIbutton Action
    @objc func dropTime(){
        
         time = time - 1
         lblOTP.text = "You will recieve OTP in \(time) sescond"
        
        if time == 0 {
            
            timer?.invalidate()
            timer = nil
            time = 0
            btnResend.isHidden = false
            lblOTP.isHidden = true
            
        }else{
            
            btnResend.isHidden = true
            lblOTP.isHidden = false
            
        }
    }
    
    @IBAction func btnActionVerify(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "loginVC") as? loginVC else {return}
        self.navigationController?.show(vc, sender: nil)
        
    }
    
    
    @IBAction func btnActionChangeNumber(_ sender: Any) {
     
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "changeMobileNumberVC") as? changeMobileNumberVC else {return}
        self.navigationController?.show(vc, sender: nil)
        
    }
    
    @IBAction func btnActionResend(_ sender: Any) {
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(dropTime), userInfo: nil, repeats: true)
        time = 60
    }
}
