//
//  mobileNumberRegisterPopVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class mobileNumberRegisterPopVC: UIViewController {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        
        btnNo.layer.cornerRadius = btnNo.frame.height / 2
        btnNo.clipsToBounds = true
        
        btnYes.layer.cornerRadius = btnYes.frame.height / 2
        btnYes.clipsToBounds = true
        
    }
    
    //MARK:- UIButton Action
    @IBAction func btnActionYes(_ sender: Any) {
    }
    
    @IBAction func btnActionNo(_ sender: Any) {
    }
    
    
    

}
