//
//  signUpVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class signUpVC: UIViewController {

    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnTermsNcondition: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnContinue.layer.cornerRadius = btnContinue.frame.height / 2
        btnContinue.clipsToBounds = true
        
        textView.layer.cornerRadius = textView.frame.height / 2
        textView.clipsToBounds = true
        
        textView.giveBottomShadow(color: UIColor.darkGray)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.btnContinue.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    //MARK:- UIbutton Action
    @IBAction func btnActionContinue(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "walkThroughVC") as? walkThroughVC else {return}
        self.navigationController?.show(vc, sender: nil)
    }
    
    @IBAction func btnActionTermsNcondition(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "termsNconditionsVC") as? termsNconditionsVC else {return}
        self.navigationController?.show(vc, sender: nil)

    }
    
    
    
}
