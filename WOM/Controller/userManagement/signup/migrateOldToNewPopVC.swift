//
//  migrateOldToNewPopVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class migrateOldToNewPopVC: UIViewController {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btncancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.clipsToBounds = true
        
        btnOk.layer.cornerRadius = btnOk.frame.height / 2
        btnOk.clipsToBounds = true
        
        btncancel.layer.cornerRadius = btncancel.frame.height / 2
        btncancel.clipsToBounds = true
        
    }
    

    //MARK:- UIbutton Button
    @IBAction func btnActionOk(_ sender: Any) {
        
        
    }
    
    @IBAction func btnActionCancel(_ sender: Any) {
        
        
        
    }
    
    

}
