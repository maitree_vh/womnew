//
//  friendsOnWomVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class friendsOnWomVC: UIViewController {

    @IBOutlet weak var objFriends: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setUP(nav: self, titleName: "FRIENDS ON WOM", isSideMenu: false)
       objFriends.register(UINib.init(nibName: "friendsOnWomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        objFriends.tableFooterView = UIView()
        
        btnContinue.layer.cornerRadius = btnContinue.frame.height / 2
        btnContinue.clipsToBounds = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnContinue.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    
    //MARK:- UIbutton Action
    @IBAction func btnActionContinue(_ sender: Any) {
     
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "inviteFriendsVC") as? inviteFriendsVC else {return}
        self.navigationController?.show(vc, sender: nil)
        
    }

}


//MARK:- UItableview datasource & delegate methods
extension friendsOnWomVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? friendsOnWomCell else {return UITableViewCell()}
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    
}

