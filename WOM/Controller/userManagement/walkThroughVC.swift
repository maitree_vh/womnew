//
//  walkThroughVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit
import SDWebImage

class walkThroughVC: UIViewController {

    var timer : Timer?
    var index = 0
    var imageArray = [#imageLiteral(resourceName: "icon_shadow"),#imageLiteral(resourceName: "OTP_loading_01"),#imageLiteral(resourceName: "OTP_loading_02")]
    var textArray = ["With WOM,","See & Share ","Let's"]
    var textArray1 = ["  NEVER ALONE    in","  RECOMMENDATIONS  ","  RECOMMEND!!  "]
    var textArray2 = ["You are macking decisions.","of Restaurants, Pubs, Movies,\n\n Places, Hotels, etc.",""]
    var rangeOfText = ["  NEVER ALONE  ","  RECOMMENDATIONS  ","  RECOMMEND!!  "]
    
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var btnChangeNumber: UIButton!
    @IBOutlet weak var objWaqlkThrough: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(scrollAutomatic), userInfo: nil, repeats: true)
        
        runGif()
        
    }
    
    func runGif(){
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "loading", withExtension: "gif")!)
        let advTimeGif = UIImage.sd_image(withGIFData: imageData)
        imgGif.image = advTimeGif!
        
    }
    
    
    //MARK:- uibutton Action
    @IBAction func btnActionChangeNumber(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "changeMobileNumberVC") as? changeMobileNumberVC else {return}
        self.navigationController?.show(vc, sender: nil)
        
    }
    
    
    @objc func scrollAutomatic(){
        
        index = index + 1
        
        if index < imageArray.count {
            
            self.objWaqlkThrough.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .right, animated: true)
            
        }else{
            
            self.timer?.invalidate()
            self.timer = nil
            
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "otpVerificationVC") as? otpVerificationVC else {return}
            self.navigationController?.show(vc, sender: nil)
            
        }
    }
}

//MARK:- UIcollectionview datasource & delegate methods
extension walkThroughVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? walkThroughCell else {return UICollectionViewCell()}
        cell.imgScroll.image = imageArray[indexPath.row]
        
        let attributeText = NSMutableAttributedString(string:textArray1[indexPath.row])
        
        let range = (textArray1[indexPath.row] as NSString).range(of: rangeOfText[indexPath.row])
        attributeText.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.orange, range: range)
        attributeText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        
        
        cell.lblMessage.text = textArray[indexPath.row]
        cell.lblMessage1.attributedText = attributeText
        cell.lblMessage2.text = textArray2[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    
}

//MARK:- uicollectionview custom class cell
class walkThroughCell : UICollectionViewCell {
    
    @IBOutlet weak var imgScroll: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblMessage1: UILabel!
    @IBOutlet weak var lblMessage2: UILabel!
    
}
