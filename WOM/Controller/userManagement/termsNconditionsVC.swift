//
//  termsNconditionsVC.swift
//  WOM
//
//  Created by Decoder on 12/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class termsNconditionsVC: UIViewController {

    @IBOutlet weak var txtname: UITextView!
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    
    var isCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setUP(nav: self, titleName: "TERMS AND CONDITIONS", isSideMenu: false)
        
        btnContinue.layer.cornerRadius = btnContinue.frame.height / 2
        btnContinue.clipsToBounds = true
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        self.btnContinue.applyGradient(colours: [UIColor(red: 120/255, green: 181/255, blue: 250/255, alpha: 1), UIColor(red: 109/255, green: 148/255, blue: 236/255, alpha: 1),UIColor(red: 90/255, green: 89/255, blue: 211/255, alpha: 1)])
        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    //MARK:- UIbutton Action
    @IBAction func btnActionContinue(_ sender: Any) {
    }
    
    @IBAction func btnActionCheck(_ sender: Any) {
        
        if isCheck{
            btnCheck.setImage(#imageLiteral(resourceName: "check-fill"), for: .normal)
            isCheck = false
        }else{
            btnCheck.setImage(#imageLiteral(resourceName: "check-empty"), for: .normal)
            isCheck = true
        }
    }
    
    @IBAction func btnActionAgree(_ sender: Any) {
        
        if isCheck{
            btnCheck.setImage(#imageLiteral(resourceName: "check-fill"), for: .normal)
            isCheck = false
        }else{
            btnCheck.setImage(#imageLiteral(resourceName: "check-empty"), for: .normal)
            isCheck = true
        }
    }
    

}
