//
//  Travellergrp_vc.swift
//  WOM
//
//  Created by DECODER on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class Travellergrp_vc: UIViewController {
    
    
    @IBOutlet weak var txt_description: RdtextField!
    @IBOutlet weak var lb_desciption: UILabel!
    @IBOutlet weak var txt_name: RdtextField!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var img_edit: UIImageView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var btn_add_participitant: UIButton!
    
    @IBOutlet weak var Tb_memberlists: UITableView!
    @IBOutlet weak var lb_members: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
           img_edit.layer.cornerRadius = 25/2
           img_edit.clipsToBounds = true
           
           img_user.layer.cornerRadius = 100/2
           img_user.clipsToBounds = true
           txt_name.layer.cornerRadius = 25
           txt_name.clipsToBounds = true
           txt_name.layer.borderWidth = 1
           
           txt_description.layer.cornerRadius = 25
           txt_description.clipsToBounds = true
           txt_description.layer.borderWidth = 1
           txt_description.layer.borderColor = UIColor.lightGray.cgColor
           self.txt_description.setNeedsLayout()
           
           
      //  txt_description.contentInset = UIEdgeInsets()
        Tb_memberlists.delegate = self
        Tb_memberlists.dataSource = self
        
        
        
        let nib = UINib(nibName: "tbc_members", bundle: nil)
        Tb_memberlists.register(nib, forCellReuseIdentifier: "tbc_members")
        
        
    }
    
    @IBAction func btn_participtant(_ sender: UIButton) {
        
        
    }
    
    
    
}


//MARK:- uitableview datasource & delegate methods
extension Travellergrp_vc : UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Tb_memberlists.dequeueReusableCell(withIdentifier: "tbc_members", for: indexPath) as! tbc_members
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
