//
//  ProfileVC.swift
//  WOM
//
//  Created by DECODER on 19/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit


class ProfileContainerVC: UIViewController {
    
    @IBOutlet weak var btnprofile: UIButton!
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var btnTrips: UIButton!
    @IBOutlet weak var btnBackground: UIButton!
    @IBOutlet weak var selector: UIView!
    @IBOutlet weak var ContainerView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnprofile.addTarget(self, action: #selector(openProfile(_:)), for: .touchUpInside)
        btnTimeline.addTarget(self, action: #selector(opentimeline(_:)), for: .touchUpInside)
        btnTrips.addTarget(self, action: #selector(openTrips(_:)), for: .touchUpInside)
        btnBackground.addTarget(self, action: #selector(openBackground(_:)), for: .touchUpInside)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage.init()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        
    }
    
    @objc func openProfile(_ button:UIButton)
    {
        
        let center = button.center
        let newcentert = CGPoint.init(x: center.x, y: btnprofile.frame.maxY)
        UIView.animate(withDuration: 0.2) {
            self.selector.center = newcentert
        }
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        guard let profilevc = storyboard.instantiateViewController(withIdentifier: "UserProfileVC") as? UserProfileVC else {return}
        if let vc = self.children.last
        {
            self.remove(asChildViewController: vc)
            self.addVC(asChildViewController: profilevc)
        }
        
    }
    
    @objc func opentimeline(_ button:UIButton)
    {
        let center = button.center
        let newcentert = CGPoint.init(x: center.x, y: button.frame.maxY)
        UIView.animate(withDuration: 0.2) {
            self.selector.center = newcentert
        }
        
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        guard let profilevc = storyboard.instantiateViewController(withIdentifier: "TimeLineVC") as? TimeLineVC else {return}
        if let vc = self.children.last
        {
            self.remove(asChildViewController: vc)
            self.addVC(asChildViewController: profilevc)
        }
    }
    
    @objc func openTrips(_ button:UIButton)
    {
        
        let center = button.center
        let newcentert = CGPoint.init(x: center.x, y: button.frame.maxY)
        UIView.animate(withDuration: 0.2) {
            self.selector.center = newcentert
        }
        
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        guard let profilevc = storyboard.instantiateViewController(withIdentifier: "TripsVC") as? TripsVC else {return}
        if let vc = self.children.last
        {
            self.remove(asChildViewController: vc)
            self.addVC(asChildViewController: profilevc)
        }
        
        
    }
    
    @objc func openBackground(_ button:UIButton)
    {
        
        let center = button.center
        let newcentert = CGPoint.init(x: center.x, y: button.frame.maxY)
        UIView.animate(withDuration: 0.2) {
            self.selector.center = newcentert
        }
        
       let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        guard let profilevc = storyboard.instantiateViewController(withIdentifier: "BookmarkVC") as? BookmarkVC else {return}
        if let vc = self.children.last
        {
            self.remove(asChildViewController: vc)
            self.addVC(asChildViewController: profilevc)
        }
        
    }
    
    // MARK: - Helper Methods
    private func addVC(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        
        // Add Child View as Subview
        ContainerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.ContainerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
}
