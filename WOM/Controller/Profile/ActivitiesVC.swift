//
//  ActivitiesVC.swift
//  WOM
//
//  Created by Maitree on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class ActivitiesVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var btnActivity: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var tblViewActivity: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

       tblViewActivity.register(UINib.init(nibName: "Activity_notification", bundle: nil), forCellReuseIdentifier: "activityCell")
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnActivityPressed(_ sender: Any) {
        self.btnActivity.setTitleColor(UIColor.blue, for: .normal)
        self.btnNotification.setTitleColor(UIColor.darkGray, for: .normal)
        self.lblActivity.isHidden = false
        self.lblNotification.isHidden = true
    }
    @IBAction func btnNotificationPressed(_ sender: Any) {
        self.btnActivity.setTitleColor(UIColor.darkGray, for: .normal)
        self.btnNotification.setTitleColor(UIColor.blue, for: .normal)
        self.lblActivity.isHidden = true
        self.lblNotification.isHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Today"
        }else if section == 1{
            return "Yesterday"
        }else{
            return "06-11-2019"
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        if section == 0{
            label.text =  "Today"
        }else if section == 1{
            label.text = "Yesterday"
        }else{
            label.text = "06-11-2019"
        }
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tblViewActivity.dequeueReusableCell(withIdentifier: "activityCell") as?  Activity_notification else {return UITableViewCell()}
        cell.selectionStyle = .none
        cell.imgProfile.layer.cornerRadius =  cell.imgProfile.frame.size.height / 2
        cell.imgProfile.clipsToBounds = true
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard!.instantiateViewController(withIdentifier: "TimeLineVC") as? TimeLineVC  else { return }
        
        self.present(vc, animated: true, completion: nil)
    }
    
}
