//
//  EditprofileVC.swift
//  WOM
//
//  Created by DECODER on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class EditprofileVC: UIViewController {

    @IBOutlet weak var txt_dob: RdtextField!
    @IBOutlet weak var lb_gender: UILabel!
    @IBOutlet weak var txt_gender: RdtextField!
    @IBOutlet weak var lb_dob: UILabel!
    @IBOutlet weak var txt_emailid: RdtextField!
    @IBOutlet weak var lb_emailid: UILabel!
    @IBOutlet weak var txt_mobileno: RdtextField!
    @IBOutlet weak var lb_mobileno: UILabel!
    @IBOutlet weak var txt_username: RdtextField!
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var txt_name: RdtextField!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var edit_img: UIImageView!
    @IBOutlet weak var user_img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        txt_name.layer.cornerRadius = 25
        txt_name.clipsToBounds = true
        txt_gender.layer.cornerRadius = 25
        txt_gender.clipsToBounds = true
        txt_emailid.layer.cornerRadius = 25
        txt_emailid.clipsToBounds = true
//        txt_dob.layer.CornerRadias
        txt_dob.layer.cornerRadius = 25
        txt_dob.clipsToBounds = true
        txt_mobileno.layer.cornerRadius = 25
        txt_mobileno.clipsToBounds = true
        txt_username.layer.cornerRadius = 25
        txt_username.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
