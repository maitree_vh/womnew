//
//  TripsVC.swift
//  WOM
//
//  Created by DECODER on 19/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class TripsVC: UIViewController {

    
    @IBOutlet weak var tblViewTrips: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblViewTrips.register(UINib.init(nibName: "TripCell", bundle: nil), forCellReuseIdentifier: "tripCell")
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tblViewTrips.dequeueReusableCell(withIdentifier: "tripCell") as?  TripCell else {return UITableViewCell()}
        
        cell.selectionStyle = .none
        cell.imgProfile.layer.cornerRadius =  cell.imgProfile.frame.size.height / 2
        cell.imgProfile.clipsToBounds = true
        
        return cell
    }
    
}


//MARK:- UItableview datasource & delegate methods
extension TripsVC  {}
