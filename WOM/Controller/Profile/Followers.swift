//
//  Followers.swift
//  WOM
//
//  Created by DECODER on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class Followers: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblview.delegate = self
        tblview.dataSource = self
        tblview.register(UINib.init(nibName: "FollowUnfollowcell", bundle: nil), forCellReuseIdentifier: "FollowUnfollowcell")
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblview.dequeueReusableCell(withIdentifier: "FollowUnfollowcell") as! FollowUnfollowcell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

}
