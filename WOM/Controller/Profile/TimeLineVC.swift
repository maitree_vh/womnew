//
//  TimeLineVC.swift
//  WOM
//
//  Created by Maitree on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class TimeLineVC: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    
      @IBOutlet weak var btnList: UIButton!
      @IBOutlet weak var btnGrid: UIButton!
      @IBOutlet weak var lblList: UILabel!
      @IBOutlet weak var lblGridd: UILabel!
      @IBOutlet weak var tblViewList: UITableView!
    
      var btnText = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        tblViewList.register(UINib.init(nibName: "TimeLine", bundle: nil), forCellReuseIdentifier: "timelineCell")
        tblViewList.register(UINib.init(nibName: "TimeLine_list", bundle: nil), forCellReuseIdentifier: "timeListCell")
        btnText = "list"
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnListPressed(_ sender: Any) {
      //  self.btnActivity.setTitleColor(UIColor.blue, for: .normal)
       // self.btnNotification.setTitleColor(UIColor.darkGray, for: .normal)
        self.lblList.isHidden = false
        self.lblGridd.isHidden = true
        btnText = "list"
        tblViewList.reloadData()

    }
    @IBAction func btnGridPressed(_ sender: Any) {
        //self.btnActivity.setTitleColor(UIColor.darkGray, for: .normal)
        //self.btnNotification.setTitleColor(UIColor.blue, for: .normal)
        self.lblList.isHidden = true
        self.lblGridd.isHidden = false
        btnText = "grid"
        tblViewList.reloadData()

    }

    func numberOfSections(in tableView: UITableView) -> Int {
           return 3
       }
       func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
           if section == 0{
               return "Today"
           }else if section == 1{
               return "Yesterday"
           }else{
               return "06-11-2019"
           }
    }
    
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let header = UIView()
           header.backgroundColor = UIColor.clear
           return header
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 3
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  btnText == "list"{
            guard let cell = tblViewList.dequeueReusableCell(withIdentifier: "timeListCell") as?  TimeLine_listCell else {return UITableViewCell()}
            cell.selectionStyle = .none
            return cell
        }else{
            guard let cell = tblViewList.dequeueReusableCell(withIdentifier: "timelineCell") as?  TimeLineCell else {return UITableViewCell()}
            cell.selectionStyle = .none
            GlobalViewShadow.ShadowWithoutBorder(cell.shadowView, color: UIColor.darkGray)
            cell.shadowView.layer.cornerRadius =  10
            cell.shadowView.clipsToBounds = true
            cell.groupView.layer.cornerRadius =  cell.groupView.frame.size.height / 2
            cell.groupView.clipsToBounds = true
            
            cell.womView.layer.cornerRadius =  cell.womView.frame.size.height / 2
            cell.womView.clipsToBounds = true
            
            cell.lblDay.layer.borderColor = UIColor.blue.cgColor
            cell.lblDay.layer.borderWidth = 1.5
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          if btnText == "list"{
             return 140
          }else{
             return 380
        }
    }
}
