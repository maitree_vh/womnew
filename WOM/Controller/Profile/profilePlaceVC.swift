//
//  profilePlaceVC.swift
//  WOM
//
//  Created by Decoder on 16/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit

class profilePlaceVC: UIViewController {
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblRecommendations: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblMembers: UILabel!
    @IBOutlet weak var objName: UICollectionView!
    @IBOutlet weak var objShowDetails: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        objShowDetails.register(UINib.init(nibName: "profilePlacesCell", bundle: nil), forCellReuseIdentifier: "cell")
        
       
    }
    

    
    

}

//MARK:- UIcollectionview datasource & delegate methods

extension profilePlaceVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? scrollNameCell else {return UICollectionViewCell()}
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: IndexPath.init(row: indexPath.row, section: indexPath.section)) as? scrollNameCell else {return}
        cell.hideView.isHidden = false
        cell.lblName.textColor = UIColor.init(red: 36/255, green: 119/255, blue: 237/255, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: IndexPath.init(row: indexPath.row, section: indexPath.section)) as? scrollNameCell else {return}
        cell.hideView.isHidden = true
        cell.lblName.textColor = UIColor.black
        
    }
    
}


//MARK:- UItableview datasource & delegate methods
extension profilePlaceVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TimeLineCell else {return UITableViewCell()}
        return cell
        
    }
    
}


//MARK:- UICollectionViewCell custom methods
class scrollNameCell : UICollectionViewCell {

    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var lblName: UILabel!
    
}

