//
//  UserProfileVC.swift
//  WOM
//
//  Created by DECODER on 19/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import UIKit


class UserProfileVC: UIViewController {
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var ProfileImages: UIImageView!
    @IBOutlet weak var GroupsView: UIView!
    @IBOutlet weak var ChannelsView: UIView!
    @IBOutlet weak var groupsHeight: NSLayoutConstraint!
    @IBOutlet weak var ChannelsHeight: NSLayoutConstraint!
    @IBOutlet weak var btnChannel: UIButton!
    @IBOutlet weak var btnGroups: UIButton!
    
    
    var Tripsopen = true
    var groupsopen = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProfileImages.layer.cornerRadius = ProfileImages.bounds.height/2
        ProfileImages.layer.masksToBounds = true
        groupsHeight.constant = 0
        ChannelsHeight.constant = 0
        btnChannel.addTarget(self, action: #selector(openChannel), for: .touchUpInside)
        btnGroups.addTarget(self, action: #selector(openGroups), for: .touchUpInside)
    }
    
    
    @objc func openGroups()
    {
        let height = self.view.bounds.height - (btnGroups.bounds.height + btnChannel.bounds.height)
        
        if Tripsopen{
            self.groupsHeight.constant = height
            self.ChannelsHeight.constant = 0
        }
        else
        {
            self.groupsHeight.constant = 0
        }
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        //scrollview.setContentOffset(CGPoint.init(x: self.btnChannel.frame.minX, y: self.btnGroups.frame.minY), animated: true)
        scrollview.scrollRectToVisible(CGRect.init(origin: self.btnGroups.frame.origin, size: self.scrollview.frame.size), animated: true)
        
        Tripsopen = !Tripsopen
    }
    
    @objc func openChannel()
    {
        //let frame = self.btnChannel.frame.origin
        
        let height = self.view.bounds.height - (btnChannel.bounds.height + btnChannel.bounds.height)
        
        if groupsopen{
            self.ChannelsHeight.constant = height
            self.groupsHeight.constant = 0
        }
        else
        {
            self.ChannelsHeight.constant = 0
        }
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        //scrollview.setContentOffset(CGPoint.init(x: self.btnChannel.frame.minX, y: self.btnGroups.frame.minY), animated: true)
        
        scrollview.scrollRectToVisible(CGRect.init(origin: self.btnGroups.frame.origin, size: self.scrollview.frame.size), animated: true)
        
        groupsopen = !groupsopen
    }
}
