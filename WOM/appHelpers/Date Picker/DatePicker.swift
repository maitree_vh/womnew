//
//  DatePicker.swift
//  Transferandtour
//
//  Created by DECODER on 06/12/18.
//  Copyright © 2018 Dhaval. All rights reserved.
//

import UIKit

protocol CustomeDatePickerDelegate:class {
    func DidSelect(pickerView:UIDatePicker,date:Date)
    func DidCancel(PickerView:UIDatePicker)
}

class DatePicker: UIView {
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
   
    weak var Delegate:CustomeDatePickerDelegate?
    
    override func awakeFromNib() {
        btnOK.addTarget(self, action: #selector(OK(_:)), for: .touchUpInside)
        btnCancel.addTarget(self, action: #selector(Cancel(_:)), for: .touchUpInside)
       // pickerView.locale = Locale.init(identifier: "en_US_POSIX")
        
    }
    
    @objc func Cancel(_ button:UIButton){
        Delegate?.DidCancel(PickerView: self.pickerView)
    }
    
    @objc func OK(_ button:UIButton)
    {
        Delegate?.DidSelect(pickerView: self.pickerView, date: self.pickerView.date)
    }

}
