//
//  navigationFile.swift
//  WOM
//
//  Created by Decoder on 14/11/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController{
    
    
    func setUP(nav : UIViewController ,titleName : String, isSideMenu : Bool){
        
        if isSideMenu{
            // code here when u add side menu
        }else{
            
            let left = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .done, target: self, action: #selector(btnBack(_:)))
            nav.navigationItem.backBarButtonItem = left
            nav.navigationItem.leftBarButtonItem = left
            nav.navigationItem.leftBarButtonItem?.title = nil
        }
        
        nav.navigationItem.title = titleName
        nav.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.init(name: "Roboto-Regular", size: 18)!,NSAttributedString.Key.foregroundColor : UIColor.darkGray]
    }
    
    // call btn to go back
    @objc func btnBack(_ btn : UIBarButtonItem){
        self.popViewController(animated: true)
    }

}
