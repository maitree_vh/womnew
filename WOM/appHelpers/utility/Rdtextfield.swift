//
//  RDTextField.swift
//  Zee-car
//
//  Created by mackbook on 3/12/18.
//  Copyright © 2018 VirtualHeight. All rights reserved.
//

import UIKit

@IBDesignable
class RdtextField: UITextField {
    
    @IBInspectable var leftImage:UIImage?{
        didSet{
            if let image = leftImage{
                self.leftViewMode = .always
                let image = UIImageView.init(image: image)
                image.contentMode = .scaleAspectFit
                image.frame = CGRect(x: 20, y: 15, width: 30, height: 30)
                self.leftView = image
                self.layoutIfNeeded()
            }
        }
    }

    @IBInspectable var RightImage:UIImage?{
        didSet{
            if let image = RightImage{
                self.rightViewMode = .always
                let image = UIImageView.init(image: image)
                image.contentMode = .scaleAspectFit
                image.frame = CGRect(x: 15, y: 15, width: 22, height: 22)
                self.rightView = image
                self.layoutIfNeeded()
            }
        }
    }

    @IBInspectable var padding = 20

    @IBInspectable var txtpadding:CGFloat = 40

    var CornerRadias = 8

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x = rect.origin.x + CGFloat(padding) - 5
        return rect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.rightViewRect(forBounds: bounds)
        rect.origin.x = rect.origin.x - CGFloat(padding) - 12
        return rect
    }
    
    let pading = UIEdgeInsets(top: 0, left: 30 , bottom: 0, right: 5)
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
          return bounds.inset(by: pading)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
          return bounds.inset(by: pading)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return  bounds.inset(by: pading)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 0.8
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
